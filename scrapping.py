import selenium.webdriver as webdriver
import selenium.webdriver.common.by as By
import selenium.webdriver.common.keys as Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import os
import sys
import time

def open_browser():
    #Abrimos un navegador en Google Chrome con el driver descargador
    browser = webdriver.Chrome(executable_path="./chromedriver_win32/chromedriver")
    browser.implicitly_wait(1000)
    return browser

def get_dropdown_values(dropdown):
    dropdown_values = []
    selector = Select(dropdown)
    # Waiting for the values to load
    element = WebDriverWait(browser, 
    10).until(EC.element_to_be_selected(selector.options[0]))

    options = selector.options
    for option in options:
        dropdown_values.append(option.text)

    return dropdown_values

lista_cargos = ['PRESIDENTE DE LA REPUBLICA',
                'PRIMER VICEPRESIDENTE DE LA REPUBLICA',
                'SEGUNDO VICEPRESIDENTE DE LA REPUBLICA',
                'CONGRESISTA DE LA REPUBLICA',
                'REPRESENTANTE ANTE EL PARLAMENTO ANDINO',
                'GOBERNADOR REGIONAL',
                'VICEGOBERNADOR REGIONAL',
                'ALCALDE PROVINCIAL',
                'REGIDOR PROVINCIAL',
                'ALCALDE DISTRITAL',
                'REGIDOR DISTRITAL',
                'CONSEJERO REGIONAL',
                'ACCESITARIO',
                'REGIDOR DEL CENTRO',
                'REGIDOR DE CENTRO POBLADO',
                'DIPUTADO',
                'SENADOR',
                'ALCALDE DE CENTRO POBLADO']

general_file = open("general.csv", 'w+')
universitario_file = open("universitario.csv", 'w+')
laboral_file = open("laboral.csv", 'w+')
cargo_partidario_file = open("cargo_partidario.csv", 'w+')
cargo_eleccion_popular_file = open("cargo_eleccion_popular.csv", 'w+')
renuncias_file = open("renuncias.csv", 'w+')
sentencias_file = open("sentencias.csv", 'w+')
sentencias_civiles_file = open("sentencias_civiles.csv", 'w+')
inmuebles_file = open("inmuebles.csv", 'w+')
vehiculos_file = open("vehiculos.csv", 'w+')
otros_file = open("otros.csv", 'w+')

general_file.write("dni,sexo,ape_paterno,ape_materno,nombres,fecha_nacimiento,foto_perfil")
general_file.write("pais_nacimiento,departamento_nacimiento,provincia_nacimiento,distrito_nacimiento,")
general_file.write("departamento_domicilio,provincia_domicilio,distrito_domicilio,direccion_domicilio,")
general_file.write("org_politica,distrito_electoral,primarios,primarios_concluidos,secundarios,secundarios_concluidos,")
general_file.write("estudios_tecnicos,nombre_centro_tecnico,nombre_carrera_tecnica,tecnico_concluido,")
general_file.write("estudios_no_univ,nombre_centro_no_univ,nombre_carrera_no_univ,no_univ_concluido,")
general_file.write("estudios_pos,nombre_centro_pos,especializacion_pos,pos_concluido,pos_egresado,flag_maestro,flag_doctor,pos_anho_obtencion,")
general_file.write("rem_bruta_anual_publico,rem_bruta_anual_privado,rem_bruta_anual,")
general_file.write("ren_bruta_ejer_individual_publico,ren_bruta_ejer_individual_privado,ren_bruta_ejer_individual,")
general_file.write("otros_publico,otros_privado,otros,")
general_file.write("total_ingresos\n")

universitario_file.write("dni,nombre_univ,consluido_univ,carrera_univ,egresado_univ,bachiller_univ,anho_obtencion_bachiller,titulo_profesional_univ,anho_obtencion_titulo\n")
laboral_file.write("dni,centro,ocupacion,ruc,direccion,desde_anho,hasta_anho,pais,departamento,provincia,distrito\n")
cargo_partidario_file.write("dni,org_politica,cargo_partidario,desde_cargo_partidario,hasta_cargo_partidario\n")
cargo_eleccion_popular_file.write("dni,org_politica,cargo_eleccion_popular,desde_eleccion_popular,hasta_eleccion_popular\n") #TODO: Renuncias
renuncias_file.write("dni,org_politica_renuncia,anho_renuncia")
sentencias_file.write("dni,n_expediente,fecha_sentencia_firme,organo_judicial,delito,fallo_pena,modalidad,complimiento_fallo\n")
sentencias_civiles_file.write("dni,materia_demanda,n_expediente,organo_judicial,fallo_pena")
inmuebles_file.write("dni,tipo_inmueble,pais,departamento,provincia,distrito,direccion,inscrito_sunarp,partida,ficha_tomo,valor_autovaluo\n")
vehiculos_file.write("dni,tipo_vehiculo,marca,modelo,anho,placa,caracteristicas,valor")
otros_file.write("dni,tipo_otro,descripcion,caracteristicas,valor")

browser = open_browser()
browser.get('https://plataformaelectoral.jne.gob.pe/')
search_candidates_button = browser.find_element_by_tag_name('button')
search_candidates_button.send_keys(Keys.Keys.ENTER)
time.sleep(5)
subregion_dropdown = browser.find_elements_by_tag_name('select')[3]
#subregion_dropdown_values = get_dropdown_values(subregion_dropdown)
subregion_dropdown_values = ['SELECCIONE', 'LIMA + RESIDENTES EN EL EXTRANJERO',
                            'AMAZONAS', 'ANCASH', 'APURIMAC', 'AREQUIPA', 'AYACUCHO', 
                            'CAJAMARCA', 'CALLAO', 'CUSCO', 'HUANCAVELICA', 'HUANUCO', 
                            'ICA', 'JUNIN', 'LA LIBERTAD', 'LAMBAYEQUE', 'LIMA PROVINCIAS', 
                            'LORETO', 'MADRE DE DIOS', 'MOQUEGUA', 'PASCO', 'PIURA', 'PUNO', 
                            'SAN MARTIN', 'TACNA', 'TUMBES', 'UCAYALI']

selector = Select(subregion_dropdown)
for j in range(1,len(subregion_dropdown_values)):
    selector.select_by_visible_text(subregion_dropdown_values[j])
    print(subregion_dropdown_values[j])
    search_button = browser.find_elements_by_tag_name('button')[1]
    search_button.send_keys(Keys.Keys.ENTER)
    time.sleep(7)
    parties_table = browser.find_element_by_tag_name('table')
    party_rows = parties_table.find_elements_by_xpath('//tbody[@ng-repeat="e in oResultados"]')
    if party_rows:
        print(len(party_rows))
    ini = 0
    last_len = 0
    for i in range(len(party_rows)):
        #print('i:',i)
        #print('len(party_rows)',len(party_rows))
        # print('row',party_row)
        see_candidates = party_rows[i].find_element_by_tag_name('td').find_element_by_tag_name('div')
        # print('see_candidates',see_candidates)
        see_candidates.click()

    candidates_rows = browser.find_element_by_tag_name('table').find_elements_by_xpath('//tr[@ng-repeat="d in e.listaCandidato"]')
    for candidate_row in candidates_rows:
        candidate_state = candidate_row.find_elements_by_tag_name('td')[-2]
        if candidate_state.text=='EXCLUSION' or candidate_state.text=='RENUNCIA':
            continue
        resume_link = candidate_row.find_elements_by_tag_name('td')[-1].find_element_by_tag_name('a')
        resume_link.click()
        browser.switch_to.window(browser.window_handles[1])
        personal = browser.find_element_by_xpath('//section[@id="datos_personales"]').find_elements_by_tag_name('div')[1]
        foto_perfil = personal.find_elements_by_tag_name('div')[0].find_element_by_tag_name('img').get_attribute("src")     
        personal_info = personal.find_elements_by_tag_name('div')[1].find_elements_by_tag_name('div')
        dni = personal_info[0].find_elements_by_tag_name('label')[-1].text
        sex = personal_info[1].find_elements_by_tag_name('label')[-1].text
        apellido_paterno = personal_info[2].find_elements_by_tag_name('label')[-1].text
        apellido_materno = personal_info[3].find_elements_by_tag_name('label')[-1].text
        nombres = personal_info[4].find_elements_by_tag_name('label')[-1].text
        fecha_nacimiento = personal_info[5].find_elements_by_tag_name('label')[-1].text
        print(foto_perfil)
        print(dni)
        print(sex)
        print(apellido_paterno)
        print(apellido_materno)
        print(nombres)
        print(fecha_nacimiento)
        general_file.write(dni+", "+sex+", "+apellido_paterno+", "+apellido_materno+", "+nombres+", "+fecha_nacimiento+", "+foto_perfil+", ")
        nacimiento = browser.find_element_by_xpath('//section[@id="datos_personales"]').find_elements_by_tag_name('div')[13].find_elements_by_tag_name('div')
        pais_nacimiento = nacimiento[0].find_elements_by_tag_name('label')[1].text
        departamento_nacimiento = nacimiento[1].find_elements_by_tag_name('label')[1].text
        provincia_nacimiento = nacimiento[2].find_elements_by_tag_name('label')[1].text
        distrito_nacimiento = nacimiento[3].find_elements_by_tag_name('label')[1].text
        print('nacimiento:')
        print(pais_nacimiento,departamento_nacimiento,provincia_nacimiento,distrito_nacimiento)
        general_file.write(pais_nacimiento+", "+departamento_nacimiento+", "+provincia_nacimiento+", "+distrito_nacimiento+", ")

        domicilio = browser.find_element_by_xpath('//section[@id="datos_personales"]').find_elements_by_tag_name('div')[18].find_elements_by_tag_name('div')
        departamento_domicilio = domicilio[0].find_elements_by_tag_name('label')[1].text
        provincia_domicilio = domicilio[1].find_elements_by_tag_name('label')[1].text
        distrito_domicilio = domicilio[2].find_elements_by_tag_name('label')[1].text
        direccion_domicilio = browser.find_element_by_xpath('//section[@id="datos_personales"]').find_elements_by_tag_name('div')[23].find_elements_by_tag_name('label')[1].text
        print('domicilio:')
        print(departamento_domicilio,provincia_domicilio,distrito_domicilio,direccion_domicilio)
        general_file.write(departamento_domicilio+", "+provincia_domicilio+", "+distrito_domicilio+", "+direccion_domicilio+", ")
        
        org_politica = browser.find_element_by_xpath('//section[@id="datos_personales"]').find_elements_by_tag_name('div')[25].find_elements_by_tag_name('label')[1].text
        print('org_politica:')
        print(org_politica)
        distrito_electoral = browser.find_element_by_xpath('//section[@id="datos_personales"]').find_elements_by_tag_name('div')[-1].find_elements_by_tag_name('label')[1].text
        print('distrito_electoral:')
        print(distrito_electoral)
        general_file.write(org_politica+", "+distrito_electoral+", ")
        flag_experiencia_laboral = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[0].find_elements_by_tag_name('input')
        if flag_experiencia_laboral[0].is_selected():
            print('Con experiencia laboral')
            n_exp_laboral = len(browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[0].find_elements_by_tag_name('article'))
            for i in range(n_exp_laboral):
                exp_lab = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[0].find_elements_by_tag_name('article')[i].find_elements_by_tag_name('div')
                centro = exp_lab[0].find_elements_by_tag_name('label')[1].text
                ocupacion = exp_lab[1].find_elements_by_tag_name('label')[1].text
                ruc = exp_lab[2].find_elements_by_tag_name('label')[1].text
                direccion = exp_lab[3].find_elements_by_tag_name('label')[1].text
                desde_anho = exp_lab[4].find_elements_by_tag_name('label')[1].text
                hasta_anho = exp_lab[5].find_elements_by_tag_name('label')[1].text
                pais = exp_lab[6].find_elements_by_tag_name('label')[1].text
                departamento = exp_lab[7].find_elements_by_tag_name('label')[1].text
                provincia = exp_lab[8].find_elements_by_tag_name('label')[1].text
                distrito = exp_lab[9].find_elements_by_tag_name('label')[1].text
                print('experiencia laboral')
                laboral_file.write(dni+", "+centro+", "+ocupacion+", "+ruc+", "+direccion+", "+desde_anho+", "+hasta_anho+", "+pais+", "+departamento+", "+provincia+", "+distrito+'\n')
        else:
            print('Sin exp')

        flag_educacion_basica = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('input')[0]
        flag_educacion_no_univ = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('input')[2]
        flag_educacion_univ = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('input')[4]
        
        if flag_educacion_basica.is_selected():
            primarios = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[4].text
            primarios_concluidos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[6].text
            secundarios = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[8].text
            secundarios_concluidos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[10].text
            print('basica')
            print(primarios,primarios_concluidos,secundarios,secundarios_concluidos)
            general_file.write(primarios+", "+primarios_concluidos+", "+secundarios+", "+secundarios_concluidos+", ")

        if flag_educacion_no_univ.is_selected():
            flag_tecnico = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[9].text
            centro_tecnicos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[11].text
            carrera_tecnica = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[13].text
            tecnico_concluido = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[15].text
            general_file.write(flag_tecnico+", "+centro_tecnicos+", "+carrera_tecnica+", "+tecnico_concluido+", ")
            
            flag_no_univ = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[17].text
            centro_no_univ = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[19].text
            carrera_no_univ = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[21].text
            no_univ_concluido = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[23].text
            general_file.write(flag_no_univ+", "+centro_no_univ+", "+carrera_no_univ+", "+no_univ_concluido+", ")
            
        if flag_educacion_univ.is_selected():
            registros_univ = browser.find_elements_by_xpath('//article[@class="ContenedorRegistro"]')
            for j in range(len(registros_univ)):
                registro_univ = registros_univ[j]
                universidad = registro_univ.find_elements_by_tag_name('label')[1].text
                flag_univ_concluido = registro_univ.find_elements_by_tag_name('label')[3].text
                carrera_univ = registro_univ.find_elements_by_tag_name('label')[5].text
                flag_egresado = registro_univ.find_elements_by_tag_name('label')[7].text
                flag_bachiller = registro_univ.find_elements_by_tag_name('label')[9].text
                anho_obtencion_bachiller = registro_univ.find_elements_by_tag_name('label')[11].text
                titulo_profesional = registro_univ.find_elements_by_tag_name('label')[13].text
                anho_obtencion_titulo = registro_univ.find_elements_by_tag_name('label')[15].text
                print('univ')
                print(universidad,flag_univ_concluido,carrera_univ,flag_egresado,flag_bachiller,anho_obtencion_bachiller,titulo_profesional,anho_obtencion_titulo)   
                universitario_file.write(dni+", "+universidad+", "+flag_univ_concluido+", "+carrera_univ+", "+flag_egresado+", "+flag_bachiller+", "+anho_obtencion_bachiller+", "+titulo_profesional+", "+anho_obtencion_titulo+"\n")

        flag_educacion_pos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[-15].text
        
        if flag_educacion_pos!='NO':
            centro_estudios_pos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[-13].text
            especializacion_pos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[-11].text
            flag_concluidos_pos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[-9].text
            flag_egresado_pos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[-7].text
            flag_maestro_pos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[-5].text
            flag_doctor_pos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[-3].text
            anho_obtencion_pos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[1].find_elements_by_tag_name('label')[-1].text
            general_file.write(flag_educacion_pos+", "+centro_estudios_pos+", "+especializacion_pos+", "+flag_concluidos_pos+", "+flag_egresado_pos+", "+flag_maestro_pos+", "+flag_doctor_pos+", "+anho_obtencion_pos+", ")
        else:
            general_file.write(",,,,,,,,")

        #Trayectoria partidaria
        trayectoria_partidaria = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[2]
        flag_trayectoria_partidaria = trayectoria_partidaria.find_elements_by_tag_name('input')[0]
        if flag_trayectoria_partidaria.is_selected():
            lista_cargo_partidario = trayectoria_partidaria.find_elements_by_xpath('//article[@ng-repeat="oCP in oListDataCargPart"]')
            for cargo_partidario in lista_cargo_partidario:
                cargo_partidario = cargo_partidario.find_element_by_tag_name('div').find_element_by_tag_name('div')
                organizacion_politica = cargo_partidario.find_elements_by_tag_name('label')[1].text
                cargo_org_politica = cargo_partidario.find_elements_by_tag_name('label')[3].text
                desde_cargo_org_politica = cargo_partidario.find_elements_by_tag_name('label')[5].text
                hasta_cargo_org_politica = cargo_partidario.find_elements_by_tag_name('label')[7].text
                print('cargo_partidario')
                print(organizacion_politica,cargo_org_politica,desde_cargo_org_politica,hasta_cargo_org_politica)
                cargo_partidario_file.write(dni+", "+organizacion_politica+", "+cargo_org_politica+", "+desde_cargo_org_politica+", "+hasta_cargo_org_politica+"\n")
        flag_eleccion_popular = trayectoria_partidaria.find_elements_by_tag_name('input')[2]
        if flag_eleccion_popular.is_selected():
            lista_cargo_eleccion_popular = trayectoria_partidaria.find_elements_by_xpath('//div[@ng-repeat="oCE in oListDataCargElec"]')
            for cargo_eleccion_popular in lista_cargo_eleccion_popular:
                opciones_cargo_eleccion_popular = cargo_eleccion_popular.find_element_by_tag_name('article').find_element_by_tag_name('div').find_element_by_tag_name('div').find_element_by_tag_name('div').find_elements_by_tag_name('input')
                registro_cargo_eleccion_popular = ""
                for i, opcion_cargo_eleccion_popular in enumerate(opciones_cargo_eleccion_popular):
                    if opcion_cargo_eleccion_popular.is_selected():
                        registro_cargo_eleccion_popular = lista_cargos[i]
                labels_cargo_eleccion_popular = cargo_eleccion_popular.find_element_by_tag_name('article').find_elements_by_tag_name('div')[20]
                org_eleccion_popular = labels_cargo_eleccion_popular.find_elements_by_tag_name('label')[-5].text
                desde_cargo_eleccion_popular = labels_cargo_eleccion_popular.find_elements_by_tag_name('label')[-3].text
                hasta_cargo_eleccion_popular = labels_cargo_eleccion_popular.find_elements_by_tag_name('label')[-1].text
                
                print('cargo_eleccion_popular')
                print('registro_cargo_eleccion_popular',registro_cargo_eleccion_popular)
                print('org_eleccion_popular',org_eleccion_popular)
                print('desde_cargo_eleccion_popular',desde_cargo_eleccion_popular)
                print('hasta_cargo_eleccion_popular',hasta_cargo_eleccion_popular)
                cargo_eleccion_popular_file.write(dni+", "+org_eleccion_popular+", "+registro_cargo_eleccion_popular+", "+desde_cargo_eleccion_popular+", "+hasta_cargo_eleccion_popular+"\n")
        #Renuncias a otros partidos
        renuncias_otros_partidos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[3]
        flag_renuncias = renuncias_otros_partidos.find_element_by_tag_name('input')
        if flag_renuncias.is_selected():
            lista_renuncias = renuncias_otros_partidos.find_elements_by_xpath('//article[@ng-repeat="r in oListDataRenuncia"]')
            for renuncia in lista_renuncias:
                organizacion_renuncia = renuncia.find_elements_by_tag_name('label')[1].text
                anho_renuncia = renuncia.find_elements_by_tag_name('label')[3].text
                print('Renuncia')
                print(organizacion_renuncia,anho_renuncia)
                renuncias_file.write(dni+", "+organizacion_renuncia+", "+anho_renuncia+"\n")
        #Relacion de sentencias
        seccion_sentencias = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[4]
        flag_sentencias = seccion_sentencias.find_element_by_tag_name('input')
        if flag_sentencias.is_selected():
            lista_sentencias = seccion_sentencias.find_elements_by_xpath('//article[@ng-repeat="oSP in oListDataSentencia"]')
            for sentencia in lista_sentencias:
                n_expediente = sentencia.find_elements_by_tag_name('label')[1].text
                fecha_expediente = sentencia.find_elements_by_tag_name('label')[3].text
                organo_judicial = sentencia.find_elements_by_tag_name('label')[5].text
                delito = sentencia.find_elements_by_tag_name('label')[7].text
                fallo_pena = sentencia.find_elements_by_tag_name('label')[9].text
                modalidad = sentencia.find_elements_by_tag_name('label')[11].text
                cumplimiento_fallo = sentencia.find_elements_by_tag_name('label')[13].text
                print('sentencia')
                print(n_expediente, fecha_expediente, organo_judicial, delito, fallo_pena, modalidad, cumplimiento_fallo)
                sentencias_file.write(dni+", "+n_expediente+", "+fecha_expediente+", "+organo_judicial+", "+delito+", "+fallo_pena+", "+modalidad+", "+cumplimiento_fallo+"\n")

        #Relacion de sentencias fundadas
        seccion_sentencias_fundadas = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[5]
        flag_sentencias_fundadas = seccion_sentencias_fundadas.find_element_by_tag_name('input')
        if flag_sentencias.is_selected():
            lista_sentencias_fundadas = seccion_sentencias_fundadas.find_elements_by_xpath('//article[@ng-repeat="oSO in oListDataSentencOblig"]')
            for sentencia in lista_sentencias_fundadas:
                materia_demanda = sentencia.find_elements_by_tag_name('label')[1].text
                n_expediente = sentencia.find_elements_by_tag_name('label')[3].text
                organo_judicial = sentencia.find_elements_by_tag_name('label')[5].text
                fallo_pena = sentencia.find_elements_by_tag_name('label')[7].text
                print('sentencia fundada')
                print(materia_demanda, n_expediente, organo_judicial, fallo_pena)
                sentencias_civiles_file.write(dni+", "+materia_demanda+", "+n_expediente+", "+organo_judicial+", "+fallo_pena+"\n")
        #Declaracion jurada de ingresos de bienes y rentas
        seccion_ingresos = browser.find_elements_by_xpath('//section[@class="experiencia_laboral SeccionHoja"]')[6]
        flag_ingresos = seccion_ingresos.find_element_by_tag_name('input')
        if flag_ingresos.is_selected():
            tabla_ingresos = seccion_ingresos.find_element_by_xpath('//table[@class="tablas-estilos alineado-izquierda"]').find_element_by_tag_name('tbody')
            rem_bruta_anual_publico = float(tabla_ingresos.find_elements_by_tag_name('tr')[0].find_elements_by_tag_name('td')[1].find_element_by_tag_name('label').text)
            rem_bruta_anual_privado = float(tabla_ingresos.find_elements_by_tag_name('tr')[0].find_elements_by_tag_name('td')[2].find_element_by_tag_name('label').text)
            rem_bruta_anual = rem_bruta_anual_publico + rem_bruta_anual_privado
            ren_bruta_ejer_individual_publico = float(tabla_ingresos.find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[1].find_element_by_tag_name('label').text)
            ren_bruta_ejer_individual_privado = float(tabla_ingresos.find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[2].find_element_by_tag_name('label').text)
            ren_bruta_ejer_individual = ren_bruta_ejer_individual_publico + ren_bruta_ejer_individual_privado
            otros_publico = float(tabla_ingresos.find_elements_by_tag_name('tr')[2].find_elements_by_tag_name('td')[1].find_element_by_tag_name('label').text)
            otros_privado = float(tabla_ingresos.find_elements_by_tag_name('tr')[2].find_elements_by_tag_name('td')[2].find_element_by_tag_name('label').text)
            otros = otros_publico + otros_privado
            total_ingresos = rem_bruta_anual + ren_bruta_ejer_individual + otros
            print('Ingresos')
            print(rem_bruta_anual_publico,rem_bruta_anual_privado,rem_bruta_anual)
            print(ren_bruta_ejer_individual_publico,ren_bruta_ejer_individual_privado,ren_bruta_ejer_individual)
            print(otros_publico,otros_privado,otros)
            print(total_ingresos)
            general_file.write(str(rem_bruta_anual_publico)+", "+str(rem_bruta_anual_privado)+", "+str(rem_bruta_anual)+", ")
            general_file.write(str(ren_bruta_ejer_individual_publico)+", "+str(ren_bruta_ejer_individual_privado)+", "+str(ren_bruta_ejer_individual)+", ")
            general_file.write(str(otros_publico)+", "+str(otros_privado)+", "+str(otros)+", ")
            general_file.write(str(total_ingresos)+"\n")
            
        #Tabla de inmuebles    
        flag_inmuebles = seccion_ingresos.find_elements_by_tag_name('input')[2]
        if flag_inmuebles.is_selected():
            table_inmuebles = seccion_ingresos.find_element_by_xpath('//article[@id="BienesInmuebles"]').find_elements_by_tag_name('div')[3].find_element_by_tag_name('table').find_element_by_tag_name('tbody')
            filas_inmuebles = table_inmuebles.find_elements_by_tag_name('tr')
            for fila_inmueble in filas_inmuebles:
                tipo_de_bien = fila_inmueble.find_elements_by_tag_name('label')[0].text
                pais = fila_inmueble.find_elements_by_tag_name('label')[1].text
                departamento = fila_inmueble.find_elements_by_tag_name('label')[2].text
                provincia = fila_inmueble.find_elements_by_tag_name('label')[3].text
                distrito = fila_inmueble.find_elements_by_tag_name('label')[4].text
                direccion = fila_inmueble.find_elements_by_tag_name('label')[5].text
                inscrito_sunarp = fila_inmueble.find_elements_by_tag_name('label')[6].text
                partida = fila_inmueble.find_elements_by_tag_name('label')[7].text
                ficha = fila_inmueble.find_elements_by_tag_name('label')[8].text
                autovaluo = float(fila_inmueble.find_elements_by_tag_name('label')[9].text)
                print('inmueble')
                print(tipo_de_bien,pais,departamento,inscrito_sunarp,autovaluo)
                inmuebles_file.write(dni+", "+tipo_de_bien+", "+pais+", "+departamento+", "+provincia+", "+distrito+", "+direccion+", "+inscrito_sunarp+", "+partida+", "+ficha+", "+str(autovaluo)+"\n")
        #Tabla de muebles    
        flag_muebles = seccion_ingresos.find_elements_by_tag_name('input')[4]
        if flag_muebles.is_selected():
            table_vehiculos = seccion_ingresos.find_element_by_xpath('//article[@id="BienesMuebles"]').find_elements_by_xpath('//table[@class="tablas-estilos alineado-centroa"]')[0].find_element_by_tag_name('tbody')
            filas_vehiculos = table_vehiculos.find_elements_by_tag_name('tr')
            for fila_vehiculo in filas_vehiculos:
                tipo_vehiculo = fila_vehiculo.find_elements_by_tag_name('label')[0].text
                marca_vehiculo = fila_vehiculo.find_elements_by_tag_name('label')[1].text
                modelo_vehiculo = fila_vehiculo.find_elements_by_tag_name('label')[2].text
                anho_vehiculo = fila_vehiculo.find_elements_by_tag_name('label')[3].text
                placa_vehiculo = fila_vehiculo.find_elements_by_tag_name('label')[4].text
                caracteristicas_vehiculo = fila_vehiculo.find_elements_by_tag_name('label')[5].text
                valor_vehiculo = fila_vehiculo.find_elements_by_tag_name('label')[6].text
                print('vehiculo')
                print(tipo_vehiculo,marca_vehiculo,modelo_vehiculo,anho_vehiculo,placa_vehiculo,valor_vehiculo)
                vehiculos_file.write(dni+", "+tipo_vehiculo+", "+marca_vehiculo+", "+modelo_vehiculo+", "+anho_vehiculo+", "+placa_vehiculo+", "+caracteristicas_vehiculo+", "+valor_vehiculo+"\n")
            table_otros = seccion_ingresos.find_element_by_xpath('//article[@id="BienesMuebles"]').find_elements_by_xpath('//table[@class="tablas-estilos alineado-centro"]')[0].find_element_by_tag_name('tbody')
            filas_otros = table_otros.find_elements_by_tag_name('tr')
            for fila_otro in filas_otros:
                tipo_otro = fila_otro.find_elements_by_tag_name('label')[0].text
                descripcion_otro = fila_otro.find_elements_by_tag_name('label')[1].text
                caracteristicas_otro = fila_otro.find_elements_by_tag_name('label')[2].text
                valor_otro = fila_otro.find_elements_by_tag_name('label')[3].text
                print('otro')
                print(tipo_otro,descripcion_otro,caracteristicas_otro,valor_otro)
                otros_file.write(dni+", "+tipo_otro+", "+descripcion_otro+", "+caracteristicas_otro+", "+valor_otro+"\n")
        browser.close()
        browser.switch_to.window(browser.window_handles[0])

general_file.close()
laboral_file.close()
cargo_partidario_file.close()
cargo_eleccion_popular_file.close()
sentencias_file.close()
sentencias_civiles_file.close()
inmuebles_file.close()
vehiculos_file.close()
otros_file.close()